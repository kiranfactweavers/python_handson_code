list_1 = [5, 3, 7, 9, 8, 11, 17]
list_2 = [52, 33, 72, 35, 87]


def sort_list(data):
    n = len(data)
    for i in range(0, n - 1):
        for j in range(0, n - i - 1):
            if data[j] < data[j + 1]:
                temp = data[j]
                data[j] = data[j + 1]
                data[j + 1] = temp


sort_list(list_1)
sort_list(list_2)
new_list = list_1 + list_2

print(new_list)
