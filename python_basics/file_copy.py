import logging

read_filename = "read_file"
write_filename = "write_file.txt"


def read_file(file_name):
    try:
        with open(file_name, "r") as file_object:
            return file_object.readlines()
    except FileNotFoundError:
        raise Exception("File Not FoundError for Read")


def write_content(file_name, content):
    with open(file_name, "w") as file_object:
        file_object.writelines(content)


def main():
    try:
        content = read_file(read_filename)
        write_content(write_filename, content)
    except Exception as e:
        logging.error(f'Error occurred while file operation --> {str(e)}')


if __name__ == "__main__":
    main()
