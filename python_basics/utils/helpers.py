import numbers

def get_value(obj, key, default="NA"):
    if isinstance(obj, dict):
        val = obj.get(key, default)
        if(type(val) == bool or isinstance(val, numbers.Number)):
            return val
        else:
            return val or default
    else:
        return default
