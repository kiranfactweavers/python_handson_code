import logging

logging.basicConfig(filename="odd_even.log", level=logging.DEBUG)

count = int(input("Please enter total numbers : "))

logging.debug("User input for count %s", str(count))

counter = 1

numbers = []

while counter <= count:
    numbers.append(int(input("Number " + str(counter) + " : ")))
    counter = counter + 1

odd = []
even = []

for number in numbers:
    if number % 2 == 0:
        even.append(number)
    else:
        odd.append(number)

logging.debug("Derived odd numbers %s", str(odd))
logging.debug("Derived even numbers %s", str(even))