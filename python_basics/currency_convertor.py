import requests
from utils.helpers import get_value

conversion_api_endpoint = "https://api.exchangerate-api.com/v4/latest/"

currenty_list = [
    "ARS",
    "AUD",
    "BGN",
    "BRL",
    "BSD",
    "CAD",
    "CHF",
    "CLP",
    "CNY",
    "COP",
    "CZK",
    "DKK",
    "DOP",
    "EGP",
    "EUR",
    "FJD",
    "GBP",
    "GTQ",
    "HKD",
    "HRK",
    "HUF",
    "IDR",
    "ILS",
    "INR",
    "ISK",
    "JPY",
    "KRW",
    "KZT",
    "MXN",
    "MYR",
    "NOK",
    "NZD",
    "PAB",
    "PEN",
    "PHP",
    "PKR",
    "PLN",
    "PYG",
    "RON",
    "RUB",
    "SAR",
    "SEK",
    "SGD",
    "THB",
    "TRY",
    "TWD",
    "UAH",
    "USD",
    "UYU",
    "VND",
    "ZAR"
]

print("\nSelect the desired conversion currencies")

for item in currenty_list:
    print("\n" + item)

print("\n")

selected_1 = input("Select the input currency : ")

selected_2 = input("\nSelect the desired conversion currency : ")

value = input("\nAmount : ")


response = requests.get(conversion_api_endpoint + selected_1)

rate = get_value(get_value(response.json(), 'rates'), selected_2)

print("\n1 " + selected_1 + " = " + str(rate) + " "+selected_2)

print("\nConverted value : " + str(float(value) * float(rate)))
