import requests
from utils.helpers import get_value

API_URL = "https://reqres.in/api/users?page=2"

response = requests.get(API_URL)

for user in get_value(response.json(), "data", []):
    print(get_value(user, "first_name", "NA") + " " + get_value(user, "last_name", "NA"))

print("***END***")