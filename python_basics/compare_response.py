import logging
import csv
import requests
import pandas as pd

from utils.helpers import get_value

filename = "request_status.csv"
API_URL = "https://api.exchangerate-api.com/v4/latest/"


def make_request():
    response = requests.get(API_URL)
    return response.status_code


def write_content(status):
    csv_object = pd.read_csv(filename)
    csv_object.loc[0, 'COMPARE_STATUS'] = status
    csv_object.to_csv(filename, index=False)


def read_file(file_name):
    try:
        with open(file_name) as csv_object:
            reader = csv.DictReader(csv_object)
            return next(reader)
    except FileNotFoundError:
        raise Exception("File Not FoundError for Read")


def compare(api_status, csv_data):
    if int(api_status) == int(get_value(csv_data, 'API_STATUS')):
        return True
    else:
        return False


def main():
    try:
        csv_content = read_file(filename)
        status_code = make_request()
        status = compare(status_code, csv_content)
        write_content(status)
    except Exception as e:
        logging.error(f'Error occurred while file operation --> {str(e)}')


if __name__ == "__main__":
    main()
