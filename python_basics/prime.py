import logging

logging.basicConfig(filename="prime.log", level=logging.DEBUG)

number = int(input("Enter your number : "))

flag = 0
if number > 1:
    for divisor in range(2, int(number / 2) + 1):
        logging.debug(f' doing {divisor} % {number} = {divisor % number}')
        if number % divisor == 0:
            flag = 1
            break
else:
    flag = 1

if flag == 1:
    logging.debug(f' {number} is not prime')
else:
    logging.debug(f' {number} is prime')

