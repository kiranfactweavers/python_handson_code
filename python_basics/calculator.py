from utils.helpers import get_value

digit_1 = int(input("Enter digit 1 : "))

digit_2 = int(input("Enter digit 2: "))

op = input(
    "Enter operation name : \n 1 - Add \n 2 - Subract \n 3 - Multiply \n 4 - Divide \n Operation : ")


def op_definition(todo):
    op_types = {
        "1": {
            "op_def": digit_1 + digit_2,
            "label": "Addition Result : "
        },
        "2": {
            "op_def": digit_1 - digit_2,
            "label": "Subracted Result : "
        },
        "3": {
            "op_def": digit_1 * digit_2,
            "label": "Multiplied Result : "
        },
        "4": {
            "op_def": digit_1 / digit_2,
            "label": "Divided Result : "
        },
    }
    return get_value(get_value(op_types, todo), "label") + " " + str(get_value(get_value(op_types, todo), "op_def"))

print(op_definition(op))
