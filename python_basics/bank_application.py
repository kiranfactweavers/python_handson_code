import sys

class Bank:
    def __init__(self):
        self.amount = 0

    def deposit(self, invest_amount):
        self.amount += invest_amount
        print("\n*****Amount deposited*****")

    def withdraw(self, withdraw_amount):
        if withdraw_amount <= self.amount:
            self.amount -= withdraw_amount
        else:
            print("\n*****Transaction not allowed*****")

    def balance(self):
        print(f'\nAvailable balance : {self.amount}')


def app(option, customer_object, param):
    if option == 1:
        customer_object.deposit(param)
    if option == 2:
        customer_object.withdraw(param)
    if option == 3:
        customer_object.balance()


def main(bank_customer):
    print("\nBanking \n")
    print("1: Deposit \n")
    print("2: Withdraw \n")
    print("3: Display \n")
    print("4: Exit \n")
    choice = int(input("Please Choose a option : "))
    if choice == 1:
        deposit_amount = int(input("\nEnter deposit amount : "))
        app(choice, bank_customer, deposit_amount)
    if choice == 2:
        withdraw_amount = int(input("\nEnter withdraw amount : "))
        app(choice, bank_customer, withdraw_amount)
    if choice == 3:
        app(choice, bank_customer, 0)
    if choice == 4:
        sys.exit()


if __name__ == "__main__":
    customer = Bank()
    while True:
        main(customer)
