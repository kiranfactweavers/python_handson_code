class Area:
    def display(self):
        pass


class Rectangle(Area):
    area = 0
    length = 0
    width = 0

    def __init__(self, length, width):
        self.area = 0
        self.length = length
        self.width = width

    def calc_area(self):
        self.area = self.length * self.width

    def display(self):
        print(f'Rectangle Area is {self.area}')


class Square(Area):
    area = 0
    side = 0

    def __init__(self, side):
        self.area = 0
        self.side = side

    def calc_area(self):
        self.area = self.side * self.side

    def display(self):
        print(f'Square Area is {self.area}')


def main():

    rectangle = Rectangle(10, 20)
    rectangle.calc_area()
    rectangle.display()

    square = Square(10)
    square.calc_area()
    square.display()


if __name__ == "__main__":
    main()
